package io.github.lokarzz.itunesappetiser.repository.app.model.base

data class LoadingData(
    val isLoading: Boolean? = null
)