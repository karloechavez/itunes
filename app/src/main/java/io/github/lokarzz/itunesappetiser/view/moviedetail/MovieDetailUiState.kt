package io.github.lokarzz.itunesappetiser.view.moviedetail

import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

data class MovieDetailUiState(
    val screen: AppEnum.Screen? = null, val movie: MovieData? = null
)
