package io.github.lokarzz.itunesappetiser.view.dashboard.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.github.lokarzz.itunesappetiser.R
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.databinding.ItemMovieCardBinding
import io.github.lokarzz.itunesappetiser.extensions.DateExtension.toDateFormat
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

class RvMovieCardAdapter(
    val handler: RvMovieTypesAdapter.Handler?
) :
    RecyclerView.Adapter<RvMovieCardAdapter.ViewHolder>() {

    val data = initAsyncListDiffer()


    private fun RecyclerView.Adapter<out RecyclerView.ViewHolder>.initAsyncListDiffer(): AsyncListDiffer<MovieData> {
        return AsyncListDiffer(this, object : DiffUtil.ItemCallback<MovieData>() {
            override fun areItemsTheSame(oldItem: MovieData, newItem: MovieData): Boolean {
                return oldItem.trackName == newItem.trackName
            }

            override fun areContentsTheSame(oldItem: MovieData, newItem: MovieData): Boolean {
                return oldItem == newItem
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMovieCardBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            ), handler = handler, differData = data
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.initView(data = data.currentList[position])
    }

    override fun getItemCount(): Int {
        return data.currentList.size
    }

    fun submitList(list: List<MovieData>) {
        data.submitList(list)
    }

    class ViewHolder(
        private val binding: ItemMovieCardBinding,
        private val handler: RvMovieTypesAdapter.Handler?,
        private val differData: AsyncListDiffer<MovieData>? = null,
    ) : RecyclerView.ViewHolder(binding.root) {

        private var data: MovieData? = null
        private val context = binding.root.context

        fun initView(data: MovieData) {
            this.data = data
            initBanner()
            initTrackName()
            initDescription()
            initHandler()
            initFavorite()
            initLastDayVisited()
        }

        private fun initLastDayVisited() {
            val lastVisit = data?.lastVisit ?: return
            binding.lastVisit = String.format(
                "%s\n%s",
                context.getString(R.string.last_visit),
                lastVisit.toDateFormat(AppConstants.Date.MM_DD_YYYY)
            )
        }

        private fun initFavorite() {
            val isFavorite = data?.favorite == true
            binding.incLayoutFavorite.isFavorite = isFavorite
        }

        private fun initHandler() {
            val data = data ?: return
            binding.handler = object : Handler {
                override fun onPressItem() {
                    handler?.onPressCard(data)
                }

                override fun onToggleFavorite() {
                    val differData = differData ?: return
                    handler?.onToggleFavorite(data, differData = differData)
                }
            }
        }

        private fun initTrackName() {
            val trackName = data?.trackName ?: ""
            binding.trackName = trackName
        }

        private fun initDescription() {
            val data = data ?: return
            with(data) {
                val primaryGenreName = primaryGenreName ?: ""
                val trackPrice = trackPrice ?: ""
                val currency = currency ?: ""

                binding.description = String.format(
                    "%s \n%s %s", primaryGenreName, trackPrice, currency
                )
            }
        }

        private fun initBanner() {
            binding.bannerUrl = data?.artworkUrl100 ?: ""
        }


        interface Handler {
            fun onPressItem()
            fun onToggleFavorite()
        }

    }


}