package io.github.lokarzz.itunesappetiser.constants

object AppEnum {


    enum class Screen {
        SPLASH,
        DASHBOARD,
        MOVIE_DETAIL,
        SEARCH
    }

}