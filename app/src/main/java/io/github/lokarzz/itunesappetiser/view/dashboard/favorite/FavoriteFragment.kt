package io.github.lokarzz.itunesappetiser.view.dashboard.favorite

import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import io.github.lokarzz.itunesappetiser.R
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.databinding.FragmentFavoriteBinding
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.observeUiState
import io.github.lokarzz.itunesappetiser.extensions.ViewExtensions.popUpScreen
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData
import io.github.lokarzz.itunesappetiser.view.base.BaseFragment
import io.github.lokarzz.itunesappetiser.view.base.dialog.Type1BottomDialogFragment
import io.github.lokarzz.itunesappetiser.view.dashboard.favorite.adapter.RvFavoriteCardAdapter
import io.github.lokarzz.itunesappetiser.view.moviedetail.MovieDetailsActivity

@AndroidEntryPoint
class FavoriteFragment : BaseFragment<FragmentFavoriteBinding>() {

    private var uiState: FavoriteUiState? = null
    private val viewModel by viewModels<FavoriteViewModel>()
    private val adapter by lazy {
        RvFavoriteCardAdapter()
    }

    override fun initViewBinding(container: ViewGroup?): FragmentFavoriteBinding {
        return FragmentFavoriteBinding.inflate(layoutInflater, container, false)
    }

    override fun initView() {
        observeLiveData()
        initAdapter()
        fetchMovieTypes()

    }

    private fun initHasFavorite() {
        val hasFavorites = !uiState?.movies.isNullOrEmpty()
        binding.hasFavorites = hasFavorites
    }

    private fun observeLiveData() {
        observeUiState(viewModel.uiState) {
            uiState = it
            initMovies()
            initHasFavorite()
        }
    }

    private fun fetchMovieTypes() {
        viewModel.fetchFavorites()
    }

    private fun initMovies() {
        val movies = uiState?.movies ?: return
        adapter.submitList(movies)
    }

    private fun initAdapter() {
        initAdapterHandler()
        binding.rvMovies.adapter = adapter
    }

    private fun initAdapterHandler() {
        adapter.handler = object : RvFavoriteCardAdapter.Handler {
            override fun onToggleFavorite(movieData: MovieData) {
                viewModel.removeFavorite(trackId = movieData.trackId ?: 0)
            }

            override fun onPressItem(movieData: MovieData) {
                popUpScreen(
                    MovieDetailsActivity::class.java,
                    bundleOf(AppConstants.Bundle.TRACK_ID to movieData.trackId)
                )
            }

            override fun onPressPlay() {
                showErrorMessageDialog()
            }
        }
    }

    private fun showErrorMessageDialog() {
        Type1BottomDialogFragment().also {
            it.title = getString(R.string.oh_no)
            it.description =
                String.format("%s \uD83D\uDE14", getString(R.string.this_is_a_trial_version))
            it.show(childFragmentManager, "")
        }
    }

    interface Handler

}