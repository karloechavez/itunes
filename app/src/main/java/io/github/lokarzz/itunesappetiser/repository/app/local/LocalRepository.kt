package io.github.lokarzz.itunesappetiser.repository.app.local

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import io.github.lokarzz.itunesappetiser.extensions.DateExtension.fetchCurrentTime
import io.github.lokarzz.itunesappetiser.repository.app.local.room.database.AppDatabase
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalRepository @Inject constructor(
    private val db: AppDatabase, private val preferences: DataStore<Preferences>
) {

    suspend fun insertMovies(movieTypes: List<MovieData>) {
        return withContext(Dispatchers.IO) {
            db.movieDao().insertAll(*movieTypes.toTypedArray())
        }
    }

    suspend fun fetchByTrackIds(trackIds: List<Long>): List<MovieData> {
        return withContext(Dispatchers.IO) {
            db.movieDao().fetchByTrackIds(trackIds)
        }
    }

    suspend fun fetchMovie(trackId: Long): MovieData? {
        return withContext(Dispatchers.IO) {
            val movie = db.movieDao().fetchMovie(trackId) ?: return@withContext null
            val updatedMovie = movie.copy(lastVisit = fetchCurrentTime())
            db.movieDao().updateMovie(updatedMovie)
            movie
        }
    }

    suspend fun fetchFavoriteMovies(): List<MovieData> {
        return withContext(Dispatchers.IO) {
            db.movieDao().fetchFavoriteMovies()
        }
    }

    suspend fun toggleFavorite(trackId: Long): MovieData? {
        return withContext(Dispatchers.IO) {
            val movie = db.movieDao().fetchMovie(trackId) ?: return@withContext null
            val isFavorite = movie.favorite == true
            val updatedMovie = movie.copy(favorite = !isFavorite)
            db.movieDao().updateMovie(updatedMovie)
            updatedMovie
        }
    }

    suspend fun fetchDataStore(key: String): Flow<String?> {
        return preferences.data.map { preferences -> preferences[stringPreferencesKey(key)] }
    }

    suspend fun fetchDataStoreLong(key: String): Flow<Long?> {
        return preferences.data.map { preferences -> preferences[longPreferencesKey(key)] }
    }


    suspend fun <T : Any> saveDataStore(key: String, value: T) {
        when (value) {
            is String -> {
                preferences.edit { preferences ->
                    preferences[stringPreferencesKey(key)] = value
                }
            }
            is Long -> {
                preferences.edit { preferences ->
                    preferences[longPreferencesKey(key)] = value
                }
            }
            else -> {
                // do nothing
            }
        }
    }


    suspend fun removeDataStore(key: String) {
        preferences.edit { preferences ->
            preferences.remove(stringPreferencesKey(key))
        }
    }
}