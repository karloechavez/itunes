package io.github.lokarzz.itunesappetiser.repository.app.remote.retrofit

import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.SearchMoviesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ITunesService {

    @GET("search?media=movie&country=AU")
    suspend fun searchMovies(
        @Query("term") term: String,
        @Query("limit") limit: Int? = 50
    ): Response<SearchMoviesResponse>
}