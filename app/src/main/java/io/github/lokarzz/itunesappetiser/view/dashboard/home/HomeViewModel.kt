package io.github.lokarzz.itunesappetiser.view.dashboard.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.github.lokarzz.itunesappetiser.repository.app.AppRepository
import io.github.lokarzz.itunesappetiser.repository.app.model.base.UIState
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.MovieTypes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val appRepository: AppRepository) : ViewModel() {


    private val _uiState by lazy {
        MutableStateFlow(HomeUiState())
    }
    val uiState by lazy {
        _uiState.asStateFlow()
    }

    fun fetchMovieType(type: String) {
        viewModelScope.launch {
            appRepository.fetchMovieType(type).collect { state ->
                when (state.status) {
                    UIState.Status.SUCCESS -> {
                        val response = state.data ?: return@collect
                        _uiState.update {
                            it.copy(
                                movieTypes = initMovieTypes(type, response)
                            )
                        }
                    }
                    else -> {
                    }
                }
            }
        }
    }

    fun toggleFavorite(trackId: Long) {
        viewModelScope.launch {
            appRepository.toggleFavorite(trackId).collect { state ->
                when (state.status) {
                    UIState.Status.SUCCESS -> {
                        _uiState.update {
                            it.copy(toggledFavorite = state.data)
                        }
                    }
                    else -> {
                        // do nothing
                    }
                }
            }
        }
    }

    private suspend fun initMovieTypes(
        type: String,
        response: MovieTypes
    ): List<MovieTypes> {
        return withContext(Dispatchers.Default) {
            val movieTypes = _uiState.value.movieTypes.toMutableList()
            when (val index = movieTypes.indexOfFirst { it.type == type }) {
                -1 -> {
                    movieTypes.add(response)
                }
                else -> {
                    movieTypes[index] = response
                }
            }
            movieTypes
        }
    }

    private fun setRefreshList(refreshList: Boolean) {
        viewModelScope.launch {
            _uiState.update {
                it.copy(refreshList = refreshList)
            }
        }
    }

}