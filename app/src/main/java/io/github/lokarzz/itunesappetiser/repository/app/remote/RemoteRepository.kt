package io.github.lokarzz.itunesappetiser.repository.app.remote

import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.SearchMoviesResponse
import io.github.lokarzz.itunesappetiser.repository.app.remote.retrofit.ITunesService
import retrofit2.Response
import javax.inject.Inject

class RemoteRepository @Inject constructor(private val iTunesService: ITunesService) {


    suspend fun searchMovies(term: String): Response<SearchMoviesResponse> {
        return iTunesService.searchMovies(term)
    }

    suspend fun fetchMovieType(term: String): Response<SearchMoviesResponse> {
        return iTunesService.searchMovies(term = term, limit = 5)
    }
}