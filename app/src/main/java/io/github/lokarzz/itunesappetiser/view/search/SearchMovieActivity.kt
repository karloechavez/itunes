package io.github.lokarzz.itunesappetiser.view.search

import androidx.activity.viewModels
import androidx.core.os.bundleOf
import dagger.hilt.android.AndroidEntryPoint
import io.github.lokarzz.itunesappetiser.R
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.databinding.ActivitySearchMovieBinding
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.delayBlock
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.observeUiState
import io.github.lokarzz.itunesappetiser.extensions.ViewExtensions.popUpScreen
import io.github.lokarzz.itunesappetiser.extensions.ViewExtensions.showKeyboard
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData
import io.github.lokarzz.itunesappetiser.view.base.BaseActivity
import io.github.lokarzz.itunesappetiser.view.base.dialog.Type1BottomDialogFragment
import io.github.lokarzz.itunesappetiser.view.moviedetail.MovieDetailsActivity
import io.github.lokarzz.itunesappetiser.view.search.adapter.RvSearchMovieCardAdapter

@AndroidEntryPoint
class SearchMovieActivity : BaseActivity<ActivitySearchMovieBinding>() {

    private var searchType: String? = null
    val viewModel by viewModels<SearchMovieViewModel>()
    private var uiState: SearchMovieUiState? = null
    private val adapter by lazy {
        RvSearchMovieCardAdapter()
    }

    override fun initViewBinding(): ActivitySearchMovieBinding {
        return ActivitySearchMovieBinding.inflate(layoutInflater)
    }

    override fun initView() {
        initInitialValues()
        initSearchValue()
        initKeyboard()
        initHandler()
        observeState()
        initAdapter()
        initMessage()
        initDefaultBackPress()
    }

    private fun initKeyboard() {
        val hasInitialSearch = searchType?.isNotEmpty() ?: false
        if (hasInitialSearch) return
        delayBlock(500) {
            binding.etSearch.showKeyboard()
        }
    }

    private fun initInitialValues() {
        searchType = intent.getStringExtra(AppConstants.Bundle.SEARCH_TYPE) ?: return
    }

    private fun initSearchValue() {
        val searchType = searchType ?: return
        binding.etSearch.setText(searchType)
        viewModel.onSearchFieldChange(searchType)
    }

    private fun initMessage() {
        val hasInitialSearch = searchType?.isNotEmpty() == true

        binding.message = if (hasInitialSearch) String.format(
            "%s \uD83D\uDD0D",
            getString(R.string.wait_let_me_fetch_that)
        ) else
            String.format("%s \uD83D\uDD0D", getString(R.string.is_there_anything))
    }

    private fun initAdapter() {
        binding.rvMovies.adapter = adapter

        initAdapterHandler()
    }

    private fun initAdapterHandler() {
        adapter.handler = object : RvSearchMovieCardAdapter.Handler {
            override fun onPressItem(movieData: MovieData) {
                popUpScreen(
                    MovieDetailsActivity::class.java,
                    bundleOf(AppConstants.Bundle.TRACK_ID to movieData.trackId)
                )
            }

            override fun onPressPlay(movieData: MovieData) {
                showErrorMessageDialog()
            }
        }
    }

    private fun showErrorMessageDialog() {
        Type1BottomDialogFragment().also {
            it.title = getString(R.string.oh_no)
            it.description =
                String.format("%s \uD83D\uDE14", getString(R.string.this_is_a_trial_version))
            it.show(supportFragmentManager, "")
        }
    }

    private fun observeState() {
        observeUiState(viewModel.uiState) {
            uiState = it
            initUiState()
        }
    }

    private fun initUiState() {
        initSearchLoading()
        initMovies()
        initHasMovies()
    }


    private fun initHasMovies() {
        val hasMovies = uiState?.movies?.isNotEmpty() ?: return
        binding.hasMovies = hasMovies
        if (hasMovies) {
            binding.message =
                String.format("%s \uD83E\uDD7A", getString(R.string.oh_darn_we_don_t_have_that))
        }
    }

    private fun initMovies() {
        val movies = uiState?.movies ?: return
        adapter.submitList(movies)
    }

    private fun initSearchLoading() {
        val loading = uiState?.loading ?: return
        binding.showProgress = loading
    }

    private fun initHandler() {
        binding.handler = object : Handler {
            override fun onSearch(searchValue: CharSequence) {
                viewModel.onSearchFieldChange(searchValue.toString())
            }
        }
    }

    interface Handler {
        fun onSearch(searchValue: CharSequence)
    }
}