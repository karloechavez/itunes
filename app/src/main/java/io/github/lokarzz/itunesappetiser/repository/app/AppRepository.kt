package io.github.lokarzz.itunesappetiser.repository.app

import io.github.lokarzz.itunesappetiser.extensions.ApiExtension.fetchResponse
import io.github.lokarzz.itunesappetiser.repository.app.local.LocalRepository
import io.github.lokarzz.itunesappetiser.repository.app.model.base.ApiError
import io.github.lokarzz.itunesappetiser.repository.app.model.base.LoadingData
import io.github.lokarzz.itunesappetiser.repository.app.model.base.UIState
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.MovieTypes
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.SearchMoviesResponse
import io.github.lokarzz.itunesappetiser.repository.app.remote.RemoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val remoteRepository: RemoteRepository, private val localRepository: LocalRepository
) {

    suspend fun <T : Any> saveDataStore(key: String, value: T) {
        localRepository.saveDataStore(key, value)
    }

    suspend fun fetchDataStore(key: String): Flow<String?> {
        return localRepository.fetchDataStore(key)
    }

    suspend fun fetchDataStoreLong(key: String): Flow<Long?> {
        return localRepository.fetchDataStoreLong(key)
    }

    suspend fun searchMovies(terms: String): Flow<UIState<SearchMoviesResponse>> {
        return flow {
            emit(remoteRepository.searchMovies(terms).fetchResponse())
        }.onStart { emit(UIState.loading(LoadingData(true))) }.onCompletion {
            emit(UIState.loading(LoadingData(false)))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun fetchMovieType(type: String): Flow<UIState<MovieTypes>> {
        return flow {
            val response = remoteRepository.fetchMovieType(type).fetchResponse()
            if (response.status == UIState.Status.ERROR) {
                emit(UIState.error(data = response.error))
                return@flow
            }
            val results = response.data?.results ?: return@flow
            localRepository.insertMovies(results)
            val movies = localRepository.fetchByTrackIds(results.filterNot { it.trackId == null }
                .map { it.trackId ?: 0 })
            emit(UIState.success(data = MovieTypes(type = type, response = movies)))
        }.onStart { emit(UIState.loading(LoadingData(true))) }.onCompletion {
            emit(UIState.loading(LoadingData(false)))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun fetchMovie(trackId: Long): Flow<UIState<MovieData>> {
        return flow {
            val data = localRepository.fetchMovie(trackId)
            emit(UIState.success(data = data))
        }.onStart { emit(UIState.loading(LoadingData(true))) }.onCompletion {
            emit(UIState.loading(LoadingData(false)))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun toggleFavorite(trackId: Long): Flow<UIState<MovieData>> {
        return flow {
            val data = localRepository.toggleFavorite(trackId)
            if (data == null) {
                emit(UIState.error(data = ApiError(message = "fail-favorite")))
                return@flow
            }
            emit(UIState.success(data = data))
        }.onStart { emit(UIState.loading(LoadingData(true))) }.onCompletion {
            emit(UIState.loading(LoadingData(false)))
        }.flowOn(Dispatchers.IO)
    }

    suspend fun fetchFavoriteMovies(): Flow<UIState<List<MovieData>>> {
        return flow {
            val data = localRepository.fetchFavoriteMovies()
            if (data.isEmpty()) {
                emit(UIState.error(data = ApiError(message = "fail-fetch-favorite")))
                return@flow
            }
            emit(UIState.success(data = data))
        }.onStart { emit(UIState.loading(LoadingData(true))) }.onCompletion {
            emit(UIState.loading(LoadingData(false)))
        }.flowOn(Dispatchers.IO)
    }
}