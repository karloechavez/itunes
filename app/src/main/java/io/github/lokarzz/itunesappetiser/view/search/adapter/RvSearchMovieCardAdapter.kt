package io.github.lokarzz.itunesappetiser.view.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.github.lokarzz.itunesappetiser.databinding.ItemSearchMovieCardBinding
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

class RvSearchMovieCardAdapter :
    RecyclerView.Adapter<RvSearchMovieCardAdapter.ViewHolder>() {

    val data = initAsyncListDiffer()
    var handler: Handler? = null

    private fun RecyclerView.Adapter<out RecyclerView.ViewHolder>.initAsyncListDiffer(): AsyncListDiffer<MovieData> {
        return AsyncListDiffer(this, object : DiffUtil.ItemCallback<MovieData>() {
            override fun areItemsTheSame(oldItem: MovieData, newItem: MovieData): Boolean {
                return oldItem.trackName == newItem.trackName
            }

            override fun areContentsTheSame(oldItem: MovieData, newItem: MovieData): Boolean {
                return oldItem.trackName == newItem.trackName
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemSearchMovieCardBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            ), handler
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.initView(data = data.currentList[position])
    }

    override fun getItemCount(): Int {
        return data.currentList.size
    }

    fun submitList(list: List<MovieData>) {
        data.submitList(list)
    }

    interface Handler {
        fun onPressItem(movieData: MovieData)
        fun onPressPlay(movieData: MovieData)
    }

    class ViewHolder(
        private val binding: ItemSearchMovieCardBinding,
        private val handler: RvSearchMovieCardAdapter.Handler?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        var data: MovieData? = null

        fun initView(data: MovieData) {
            this.data = data
            initBanner()
            initDescription()
            initHandler()
        }

        private fun initHandler() {
            val data = data ?: return
            binding.handler = object : Handler {
                override fun onPressItem() {
                    handler?.onPressItem(data)
                }

                override fun onPressPlay() {
                    handler?.onPressPlay(data)
                }
            }
        }


        private fun initDescription() {
            val data = data ?: return
            with(data) {
                val primaryGenreName = primaryGenreName ?: ""
                val trackName = trackName ?: ""
                val trackPrice = trackPrice ?: ""
                val currency = currency ?: ""

                binding.description = String.format(
                    "%s %s \n %s %s",
                    trackName,
                    primaryGenreName,
                    trackPrice,
                    currency
                )
            }
        }

        private fun initBanner() {
            binding.bannerUrl = data?.artworkUrl100 ?: ""
        }


        interface Handler {
            fun onPressItem()
            fun onPressPlay()
        }
    }
}