package io.github.lokarzz.itunesappetiser.repository.app.local.room.dao

import androidx.room.*
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

@Dao
interface MovieDao {

    @Query("SELECT * FROM movie")
    fun fetchMovieTypes(): List<MovieData>


    @Query("SELECT * FROM movie WHERE trackId=(:trackId)")
    fun fetchMovie(trackId : Long): MovieData?



    @Query("SELECT * FROM movie WHERE trackId IN (:trackIds)")
    fun fetchByTrackIds(trackIds: List<Long>): List<MovieData>

    @Query("SELECT * FROM movie WHERE favorite=1")
    fun fetchFavoriteMovies(): List<MovieData>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(vararg movieData: MovieData)

    @Delete
    fun deleteMovie(movieData: MovieData)

    @Update
    fun updateMovie(movieData: MovieData)

}