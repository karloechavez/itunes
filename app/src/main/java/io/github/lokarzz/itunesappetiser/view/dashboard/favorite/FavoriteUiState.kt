package io.github.lokarzz.itunesappetiser.view.dashboard.favorite

import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

data class FavoriteUiState(
    val screen: AppEnum.Screen? = null,
    val movies: List<MovieData>? = null
)
