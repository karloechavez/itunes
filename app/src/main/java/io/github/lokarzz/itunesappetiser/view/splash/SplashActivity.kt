package io.github.lokarzz.itunesappetiser.view.splash

import android.annotation.SuppressLint
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import dagger.hilt.android.AndroidEntryPoint
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.databinding.ActivitySplashBinding
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.observeUiState
import io.github.lokarzz.itunesappetiser.extensions.ViewExtensions.goToScreen
import io.github.lokarzz.itunesappetiser.view.base.BaseActivity
import io.github.lokarzz.itunesappetiser.view.dashboard.DashboardActivity
import io.github.lokarzz.itunesappetiser.view.moviedetail.MovieDetailsActivity
import io.github.lokarzz.itunesappetiser.view.search.SearchMovieActivity

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    private var uiState: SplashUiState? = null
    private val viewModel by viewModels<SplashViewModel>()

    override fun initViewBinding(): ActivitySplashBinding {
        return ActivitySplashBinding.inflate(layoutInflater)
    }

    override fun initView() {
        observeLiveData()
    }

    private fun observeLiveData() {
        observeUiState(viewModel.uiState) {
            uiState = it
            initUiState()
        }
    }

    private fun initUiState() {
        initScreen()
    }

    private fun initScreen() {
        val screen = uiState?.screen ?: return
        when (screen) {
            AppEnum.Screen.DASHBOARD.name -> {
                goToScreen(DashboardActivity::class.java)
            }
            AppEnum.Screen.SEARCH.name -> {
                goToScreen(SearchMovieActivity::class.java)
            }
            AppEnum.Screen.MOVIE_DETAIL.name -> {
                toMovieDetail()
            }
            else -> {
                // do nothing
            }
        }
    }

    private fun toMovieDetail() {
        val trackId = uiState?.trackId ?: return
        goToScreen(
            MovieDetailsActivity::class.java,
            bundleOf(AppConstants.Bundle.TRACK_ID to trackId)
        )

    }
}