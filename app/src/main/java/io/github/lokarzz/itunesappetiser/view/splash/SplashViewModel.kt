package io.github.lokarzz.itunesappetiser.view.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.repository.app.AppRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(private val appRepository: AppRepository) : ViewModel() {

    private val _uiState by lazy {
        MutableStateFlow(SplashUiState())
    }

    val uiState by lazy {
        _uiState.asStateFlow()
    }

    init {
        fetchLastScreen()
    }

    private fun fetchLastScreen() {
        viewModelScope.launch {
            appRepository.fetchDataStore(AppConstants.Preference.LAST_SCREEN).collect { state ->
                if (state == AppEnum.Screen.MOVIE_DETAIL.name) {
                    fetchTrackId()
                } else {
                    _uiState.update {
                        it.copy(screen = state ?: AppEnum.Screen.DASHBOARD.name)
                    }
                }
            }
        }
    }

    private fun fetchTrackId() {
        viewModelScope.launch {
            appRepository.fetchDataStoreLong(AppConstants.Bundle.TRACK_ID).collect { state ->
                _uiState.update {
                    it.copy(screen = AppEnum.Screen.MOVIE_DETAIL.name, trackId = state)
                }
            }
        }
    }

}