package io.github.lokarzz.itunesappetiser.view.search

import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

data class SearchMovieUiState(
    val screen: AppEnum.Screen? = null,
    val loading: Boolean = false,
    val movies: List<MovieData>? = null
)
