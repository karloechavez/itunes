package io.github.lokarzz.itunesappetiser.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ItunesApplication : Application() {
}