package io.github.lokarzz.itunesappetiser.view.dashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.repository.app.AppRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {


    private val _uiState by lazy {
        MutableStateFlow(DashboardUiState())
    }
    val uiState by lazy {
        _uiState.asStateFlow()
    }

    init {
        saveCurrentScreen()
    }

    private fun saveCurrentScreen() {
        viewModelScope.launch {
            appRepository.saveDataStore(
                AppConstants.Preference.LAST_SCREEN,
                AppEnum.Screen.DASHBOARD.name
            )
        }
    }

}