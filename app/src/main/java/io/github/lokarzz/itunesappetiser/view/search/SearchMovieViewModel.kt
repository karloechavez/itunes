package io.github.lokarzz.itunesappetiser.view.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension
import io.github.lokarzz.itunesappetiser.repository.app.AppRepository
import io.github.lokarzz.itunesappetiser.repository.app.model.base.UIState
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.SearchMoviesResponse
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchMovieViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {

    private val _uiState by lazy {
        MutableStateFlow(SearchMovieUiState())
    }
    val uiState by lazy {
        _uiState.asStateFlow()
    }

    var searchJob: Job? = null
    private val throttleLast by lazy {
        CoroutineExtension.ThrottleLast<String>(viewModelScope)
            .interval(1000)
            .subscribe {
                searchMovies(it)
            }
    }

    init {
        saveCurrentScreen()
    }

    private fun saveCurrentScreen() {
        viewModelScope.launch {
            appRepository.saveDataStore(
                AppConstants.Preference.LAST_SCREEN,
                AppEnum.Screen.SEARCH.name
            )
        }
    }

    fun onSearchFieldChange(searchValue: String) {
        throttleLast.emitValue(searchValue)
    }

    private fun searchMovies(searchValue: String) {
        searchJob?.cancel(null)
        viewModelScope.launch {
            appRepository.searchMovies(searchValue).collect { state ->
                when (state.status) {
                    UIState.Status.SUCCESS -> {
                        state.data?.let {
                            handleSearchMovieSuccess(it)
                        }
                    }
                    UIState.Status.ERROR -> {
                        handleSearchMovieError()
                    }
                    UIState.Status.LOADING -> {
                        _uiState.update {
                            it.copy(loading = state.loadingData?.isLoading == true)
                        }
                    }
                    else -> {
                        // do nothing
                    }
                }
            }
        }.also {
            searchJob = it
        }
    }

    private fun handleSearchMovieSuccess(data: SearchMoviesResponse) {
        val movies = data.results ?: return
        _uiState.update {
            it.copy(movies = movies)
        }
    }

    private fun handleSearchMovieError() {
        // TODO
    }
}