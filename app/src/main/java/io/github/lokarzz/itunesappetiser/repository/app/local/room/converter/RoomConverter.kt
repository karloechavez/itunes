package io.github.lokarzz.itunesappetiser.repository.app.local.room.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData


class RoomConverter {

    @TypeConverter
    fun toListMovieData(value: String): List<MovieData> {
        val type =
            object : TypeToken<List<MovieData?>?>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromListMovieData(data: List<MovieData>): String {
        val type =
            object : TypeToken<List<MovieData>>() {}.type
        return Gson().toJson(data, type)
    }
}