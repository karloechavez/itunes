package io.github.lokarzz.itunesappetiser.repository.app.model.movies.search

import com.google.gson.annotations.SerializedName

data class SearchMoviesResponse(

    @SerializedName("resultCount") var resultCount: Int? = null,
    @SerializedName("results") var results: List<MovieData>? = null

)