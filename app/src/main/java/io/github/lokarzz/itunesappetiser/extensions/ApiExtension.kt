package io.github.lokarzz.itunesappetiser.extensions

import io.github.lokarzz.itunesappetiser.extensions.GsonExtension.fetchData
import io.github.lokarzz.itunesappetiser.repository.app.model.base.ApiError
import io.github.lokarzz.itunesappetiser.repository.app.model.base.ErrorData
import io.github.lokarzz.itunesappetiser.repository.app.model.base.UIState
import retrofit2.Response
import java.net.UnknownHostException

object ApiExtension {

    /**
     * Fetch response
     *
     * @param T the type of a member in this function
     * @return this function return success/error UIState
     */
    fun <T> Response<T>.fetchResponse(): UIState<T> {
        return try {
            when {
                isSuccessful && body() != null -> {
                    UIState.success(body())
                }
                else -> {
                    catchError()
                }
            }
        } catch (e: Exception) {
            e.catchException()
        }
    }

    /**
     * Catch exception
     *
     * @param T the type of a member in this function
     * @return this function return error status UIState
     */
    private fun <T> Exception.catchException(): UIState<T> {
        return UIState.error(
            ApiError(
                status = when (this) {
                    is UnknownHostException -> {
                        ApiError.Status.NO_NETWORK
                    }
                    else -> {
                        ApiError.Status.UNKNOWN
                    }
                }, message = message
            )
        )
    }

    /**
     * Catch exception
     *
     * @param T the type of a member in this function
     * @return this function return api error UIState
     */
    private fun <T> Response<T>.catchError(): UIState<T> {
        val errorMessage =
            errorBody()?.charStream()?.fetchData(ErrorData::class.java)?.errorMessage ?: ""
        return UIState.error(ApiError(ApiError.Status.API, message = errorMessage, code = code()))
    }
}