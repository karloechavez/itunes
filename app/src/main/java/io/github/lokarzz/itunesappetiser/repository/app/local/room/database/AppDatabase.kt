package io.github.lokarzz.itunesappetiser.repository.app.local.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData
import io.github.lokarzz.itunesappetiser.repository.app.local.room.converter.RoomConverter
import io.github.lokarzz.itunesappetiser.repository.app.local.room.dao.MovieDao


@Database(entities = [MovieData::class], version = 4)
@TypeConverters(RoomConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}