package io.github.lokarzz.itunesappetiser.view.dashboard.home

import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.MovieTypes
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

data class HomeUiState(
    val screen: AppEnum.Screen? = null,
    val movieTypes: List<MovieTypes> = arrayListOf(),
    var toggledFavorite: MovieData? = null,
    var refreshList: Boolean? = null,

)
