package io.github.lokarzz.itunesappetiser.view.splash


data class SplashUiState(val screen: String? = null, val trackId: Long? = null)
