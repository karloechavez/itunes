package io.github.lokarzz.itunesappetiser.repository.app.model.movies

import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

data class MovieTypes(
    var type: String? = null,
    var response: List<MovieData>? = null,
)
