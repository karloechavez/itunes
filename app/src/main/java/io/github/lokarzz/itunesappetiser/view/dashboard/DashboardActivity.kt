package io.github.lokarzz.itunesappetiser.view.dashboard

import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import io.github.lokarzz.itunesappetiser.R
import io.github.lokarzz.itunesappetiser.databinding.ActivityHomeBinding
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.delayBlock
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.observeUiState
import io.github.lokarzz.itunesappetiser.extensions.ViewExtensions.popUpScreen
import io.github.lokarzz.itunesappetiser.view.base.BaseActivity
import io.github.lokarzz.itunesappetiser.view.search.SearchMovieActivity

@AndroidEntryPoint
class DashboardActivity : BaseActivity<ActivityHomeBinding>() {

    private var uiState: DashboardUiState? = null
    private val viewModel by viewModels<DashboardViewModel>()
    private val nav by lazy {
        findNavController(R.id.dashboard_nav)
    }
    private val onDestinationChangeListener by lazy {
        fetchOnDestinationChangeListener()
    }

    override fun initViewBinding(): ActivityHomeBinding {
        return ActivityHomeBinding.inflate(layoutInflater)
    }

    override fun initView() {
        observeLiveData()
        initNavigation()
        initHandler()
    }

    private fun initNavigation() {
        initNavCallBack()
    }

    private fun initNavCallBack() {
        delayBlock(500) {
            nav.addOnDestinationChangedListener(onDestinationChangeListener)
        }
    }

    private fun fetchOnDestinationChangeListener(): NavController.OnDestinationChangedListener {
        return NavController.OnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.favoriteFragment -> {
                    binding.title = getString(R.string.favorites)
                    binding.abl.setExpanded(true, true)
                }
                else -> {
                    binding.title = null
                }
            }
        }
    }


    private fun initHandler() {
        binding.handler = object : Handler {
            override fun onPressSearch() {
                popUpScreen(SearchMovieActivity::class.java)
            }

            override fun onPressFavorite() {
                if (nav.currentDestination?.id != R.id.favoriteFragment) {
                    nav.navigate(R.id.favoriteFragment)
                }
            }

            override fun onBackPress() {
                onBackPressedDispatcher.onBackPressed()
            }
        }
    }


    private fun observeLiveData() {
        observeUiState(viewModel.uiState) {
            uiState = it
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        nav.removeOnDestinationChangedListener(onDestinationChangeListener)
    }

    interface Handler {
        fun onPressSearch()
        fun onPressFavorite()
        fun onBackPress()
    }

}