package io.github.lokarzz.itunesappetiser.view.moviedetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.constants.AppEnum
import io.github.lokarzz.itunesappetiser.repository.app.AppRepository
import io.github.lokarzz.itunesappetiser.repository.app.model.base.UIState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {

    private val _uiState by lazy {
        MutableStateFlow(MovieDetailUiState())
    }
    val uiState by lazy {
        _uiState.asStateFlow()
    }

    init {
        saveCurrentScreen()
    }

    private fun saveCurrentScreen() {
        viewModelScope.launch {
            appRepository.saveDataStore(
                AppConstants.Preference.LAST_SCREEN,
                AppEnum.Screen.MOVIE_DETAIL.name
            )
        }
    }

    fun saveTrackId(trackId: Long) {
        viewModelScope.launch {
            appRepository.saveDataStore(
                AppConstants.Bundle.TRACK_ID,
                trackId
            )
        }
    }

    fun fetchMovie(trackId: Long) {
        viewModelScope.launch {
            appRepository.fetchMovie(trackId).collect { state ->
                when (state.status) {
                    UIState.Status.SUCCESS -> {
                        _uiState.update {
                            it.copy(movie = state.data)
                        }
                    }
                    else -> {
                        // do nothing
                    }
                }
            }
        }
    }

    fun toggleFavorite(trackId: Long) {
        viewModelScope.launch {
            appRepository.toggleFavorite(trackId).collect { state ->
                when (state.status) {
                    UIState.Status.SUCCESS -> {
                        _uiState.update {
                            it.copy(movie = state.data)
                        }
                    }
                    else -> {
                        // do nothing
                    }
                }
            }
        }
    }
}