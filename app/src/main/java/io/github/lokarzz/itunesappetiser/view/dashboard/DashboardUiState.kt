package io.github.lokarzz.itunesappetiser.view.dashboard

import io.github.lokarzz.itunesappetiser.constants.AppEnum

data class DashboardUiState(
    val screen: AppEnum.Screen? = null,
)
