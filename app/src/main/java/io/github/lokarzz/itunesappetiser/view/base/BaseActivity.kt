package io.github.lokarzz.itunesappetiser.view.base

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import io.github.lokarzz.itunesappetiser.extensions.ViewExtensions.goToScreen
import io.github.lokarzz.itunesappetiser.view.dashboard.DashboardActivity

abstract class BaseActivity<T : ViewBinding> : AppCompatActivity() {

    abstract fun initViewBinding(): T
    abstract fun initView()
    private var _binding: T? = null
    val binding by lazy {
        _binding!!
    }
    private val backPressCallback by lazy {
        fetchOnBackPressedCallback()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(initViewBinding()) {
            _binding = this
            setContentView(root)
        }
        initView()
    }

    fun initDefaultBackPress() {
        onBackPressedDispatcher.addCallback(backPressCallback)
    }

    private fun fetchOnBackPressedCallback(): OnBackPressedCallback {
        return object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (isTaskRoot) {
                    goToScreen(DashboardActivity::class.java)
                } else {
                    finish()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}