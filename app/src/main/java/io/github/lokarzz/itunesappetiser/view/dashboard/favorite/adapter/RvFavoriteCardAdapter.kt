package io.github.lokarzz.itunesappetiser.view.dashboard.favorite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.github.lokarzz.itunesappetiser.databinding.ItemFavoriteCardBinding
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

class RvFavoriteCardAdapter() :
    RecyclerView.Adapter<RvFavoriteCardAdapter.ViewHolder>() {

    val data = initAsyncListDiffer()
    var handler: Handler? = null

    private fun RecyclerView.Adapter<out RecyclerView.ViewHolder>.initAsyncListDiffer(): AsyncListDiffer<MovieData> {
        return AsyncListDiffer(this, object : DiffUtil.ItemCallback<MovieData>() {
            override fun areItemsTheSame(oldItem: MovieData, newItem: MovieData): Boolean {
                return oldItem.trackName == newItem.trackName
            }

            override fun areContentsTheSame(oldItem: MovieData, newItem: MovieData): Boolean {
                return oldItem.trackName == newItem.trackName
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemFavoriteCardBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            ), handler
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.initView(data = data.currentList[position])
    }

    override fun getItemCount(): Int {
        return data.currentList.size
    }

    fun submitList(list: List<MovieData>) {
        data.submitList(list)
    }

    interface Handler {
        fun onToggleFavorite(movieData: MovieData)
        fun onPressItem(movieData: MovieData)
        fun onPressPlay()
    }

    class ViewHolder(
        private val binding: ItemFavoriteCardBinding,
        private val handler: RvFavoriteCardAdapter.Handler? = null
    ) :
        RecyclerView.ViewHolder(binding.root) {

        var data: MovieData? = null


        fun initView(data: MovieData) {
            this.data = data
            initBanner()
            initTrackName()
            initDescription()
            initHandler()
            initFavorite()
        }

        private fun initFavorite() {
            val isFavorite = data?.favorite == true
            binding.incLayoutFavorite.isFavorite = isFavorite
        }

        private fun initHandler() {
            val data = data ?: return
            binding.handler = object : Handler {
                override fun onPressItem() {
                    handler?.onPressItem(data)
                }

                override fun onPressPlay() {
                    handler?.onPressPlay()
                }

                override fun onToggleFavorite() {
                    handler?.onToggleFavorite(data)
                }
            }
        }

        private fun initTrackName() {
            val trackName = data?.trackName ?: ""
            binding.trackName = trackName
        }

        private fun initDescription() {
            val data = data ?: return
            with(data) {
                val primaryGenreName = primaryGenreName ?: ""
                val trackPrice = trackPrice ?: ""
                val currency = currency ?: ""

                binding.description = String.format(
                    "%s \n %s %s",
                    primaryGenreName,
                    trackPrice,
                    currency
                )
            }
        }

        private fun initBanner() {
            binding.bannerUrl = data?.artworkUrl100 ?: ""
        }


        interface Handler {
            fun onPressItem()
            fun onPressPlay()
            fun onToggleFavorite()
        }

    }


}