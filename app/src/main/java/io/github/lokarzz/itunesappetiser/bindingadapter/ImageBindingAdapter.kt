package io.github.lokarzz.itunesappetiser.bindingadapter

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import io.github.lokarzz.itunesappetiser.R


object ImageBindingAdapter {

    /**
     * Img url
     * @see Glide
     * @param imgUrl the image  to load from
     */
    @BindingAdapter("imgUrl")
    @JvmStatic
    fun AppCompatImageView.imgUrl(imgUrl: String?) {
        imgUrl ?: return
        Glide.with(context).load(imgUrl)
            .error(R.drawable.ic_error)
            .placeholder(R.drawable.ic_itunes)
            .into(this)
    }


}