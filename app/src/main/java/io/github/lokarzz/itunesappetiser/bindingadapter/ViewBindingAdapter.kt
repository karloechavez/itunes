package io.github.lokarzz.itunesappetiser.bindingadapter

import android.view.View
import android.webkit.WebView
import androidx.databinding.BindingAdapter

object ViewBindingAdapter {

    /**
     * Show
     *
     * @param show the visibility to apply on the view
     */
    @BindingAdapter("show")
    @JvmStatic
    fun View.show(show: Boolean) {
        this.visibility = if (show) View.VISIBLE else View.GONE
    }


    /**
     * Load link
     *
     * @param link the value to load to web-view
     */
    @BindingAdapter("loadLink")
    @JvmStatic
    fun WebView.loadLink(link: String? = "") {
        with(settings) {
            useWideViewPort = true
            loadWithOverviewMode = true
            domStorageEnabled = true
        }
        link?.let { loadUrl(it) }
    }

    /**
     * Rotate
     *
     * @param toRotate the value to rotate the view
     */
    @BindingAdapter("rotate")
    @JvmStatic
    fun View.rotate(toRotate: Float? = null) {
        animate().rotation(toRotate ?: 0f).duration = 250
    }
}