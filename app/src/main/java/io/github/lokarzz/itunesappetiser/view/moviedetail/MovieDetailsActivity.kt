package io.github.lokarzz.itunesappetiser.view.moviedetail

import android.text.TextUtils
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import io.github.lokarzz.itunesappetiser.R
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.databinding.ActivityMovieDetailsBinding
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.observeUiState
import io.github.lokarzz.itunesappetiser.extensions.DateExtension.toDateFormat
import io.github.lokarzz.itunesappetiser.view.base.BaseActivity
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class MovieDetailsActivity : BaseActivity<ActivityMovieDetailsBinding>() {

    private var uiState: MovieDetailUiState? = null
    private val viewModel by viewModels<MovieDetailsViewModel>()
    private var trackId: Long? = null

    override fun initViewBinding(): ActivityMovieDetailsBinding {
        return ActivityMovieDetailsBinding.inflate(layoutInflater)
    }

    override fun initView() {
        fetchMovie()
        saveTrackId()
        observeLiveData()
        initDefaultBackPress()
    }


    private fun observeLiveData() {
        observeUiState(viewModel.uiState) {
            this.uiState = it
            initUiState()
        }

    }

    private fun initUiState() {
        initHandler()
        initLink()
        initTitle()
        initDescription()
        initToggle()
        initMovieDetails()
        initFavorite()
    }

    private fun saveTrackId() {
        val trackId = trackId ?: return
        viewModel.saveTrackId(trackId)
    }


    private fun fetchMovie() {
        trackId = intent.getLongExtra(AppConstants.Bundle.TRACK_ID, 0)
        if (trackId != 0L) {
            viewModel.fetchMovie(trackId ?: 0)
        } else {
            finish()
        }
    }

    private fun initFavorite() {
        val isFavorite = uiState?.movie?.favorite ?: return
        binding.incLayoutFavorite.isFavorite = isFavorite
    }

    private fun initMovieDetails() {
        val movieData = uiState?.movie ?: return
        with(movieData) {
            val movieDetail = listOf(
                fetchPrice(),
                primaryGenreName,
                contentAdvisoryRating,
                trackTimeMillis?.trackTimeHourMin(),
                releaseDate?.toDateFormat(AppConstants.Date.MM_YYYY)
            ).filterNot { it == null }
            binding.movieDetails = TextUtils.join(" • ", movieDetail)
        }
    }

    private fun fetchPrice(): String? {
        val price = listOf(fetchTrackPrice(), trackRentalPrice()).filterNot { it == null }
        return TextUtils.join(" / ", price)
    }

    private fun fetchTrackPrice(): String? {
        val trackPrice = uiState?.movie?.trackPrice ?: return null
        val currency = uiState?.movie?.currency ?: return null
        return String.format("%s %s %s", getString(R.string.buy), trackPrice, currency)
    }

    private fun trackRentalPrice(): String? {
        val trackRentalPrice = uiState?.movie?.trackRentalPrice ?: return null
        val currency = uiState?.movie?.currency ?: return null
        return String.format("%s %s %s", getString(R.string.rent), trackRentalPrice, currency)

    }

    private fun Long.trackTimeHourMin(): String? {
        val hours = TimeUnit.MILLISECONDS.toHours(this)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(this - TimeUnit.HOURS.toMillis(hours))
        val array = ArrayList<String>()
        if (hours > 0) {
            array.add(String.format("%s %s", hours, getString(R.string.hour)))
        }
        if (minutes > 0) {
            array.add(String.format("%s %s", minutes, getString(R.string.minutes)))
        }
        val value = TextUtils.join(" ", array)
        return value.ifEmpty { null }
    }

    private fun initToggle() {
        val longDescription = uiState?.movie?.longDescription ?: ""
        binding.showToggle = longDescription.length >= 100
    }

    private fun initHandler() {
        binding.handler = object : Handler {

            override fun onToggleDescription() {
                this@MovieDetailsActivity.onToggleDescription()
                initDescription()
            }

            override fun onToggleFavorite() {
                this@MovieDetailsActivity.onToggleFavorite()
            }
        }
    }

    private fun onToggleFavorite() {
        val trackId = trackId ?: return
        viewModel.toggleFavorite(trackId)
    }

    private fun onToggleDescription() {
        val isToggled = binding.isToggled ?: false
        binding.isToggled = !isToggled
    }

    private fun initDescription() {
        val description = uiState?.movie?.longDescription ?: return
        val isToggled = binding.isToggled ?: false
        binding.description = when {
            description.length <= 100 || isToggled -> {
                description
            }
            else -> {
                String.format("%s...", description.substring(0, 100))

            }
        }
    }

    private fun initTitle() {
        val trackName = uiState?.movie?.trackName ?: return
        binding.trackName = trackName
    }

    private fun initLink() {
        val previewUrl = uiState?.movie?.previewUrl ?: return
        binding.link = previewUrl
    }

    interface Handler {
        fun onToggleDescription()
        fun onToggleFavorite()
    }


}