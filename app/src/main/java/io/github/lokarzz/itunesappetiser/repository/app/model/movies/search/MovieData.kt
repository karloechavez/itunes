package io.github.lokarzz.itunesappetiser.repository.app.model.movies.search

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "movie")
data class MovieData(

    @PrimaryKey @SerializedName("trackId") var trackId: Long? = null,
    @SerializedName("trackName") var trackName: String? = null,
    @SerializedName("primaryGenreName") var primaryGenreName: String? = null,
    @SerializedName("trackPrice") var trackPrice: Float? = null,
    @SerializedName("currency") var currency: String? = null,
    @SerializedName("artworkUrl100") var artworkUrl100: String? = null,
    @SerializedName("previewUrl") var previewUrl: String? = null,
    @SerializedName("longDescription") var longDescription: String? = null,
    @SerializedName("contentAdvisoryRating") var contentAdvisoryRating: String? = null,
    @SerializedName("trackTimeMillis") var trackTimeMillis: Long? = null,
    @SerializedName("releaseDate") var releaseDate: String? = null,
    @SerializedName("trackRentalPrice") var trackRentalPrice: Float? = null,
    @SerializedName("favorite") var favorite: Boolean? = null,
    @SerializedName("lastVisit") var lastVisit: String? = null,

    ) : Parcelable