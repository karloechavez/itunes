package io.github.lokarzz.itunesappetiser.view.dashboard.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.github.lokarzz.itunesappetiser.R
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.databinding.ItemMovieTypesBinding
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.MovieTypes
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData

class RvMovieTypesAdapter : RecyclerView.Adapter<RvMovieTypesAdapter.ViewHolder>() {

    val data by lazy {
        initAsyncListDiffer()
    }

    var handler: Handler? = null

    private fun initAsyncListDiffer(): AsyncListDiffer<MovieTypes> {

        return AsyncListDiffer(this, object : DiffUtil.ItemCallback<MovieTypes>() {
            override fun areItemsTheSame(oldItem: MovieTypes, newItem: MovieTypes): Boolean {
                return oldItem.type.equals(newItem.type)
            }

            override fun areContentsTheSame(oldItem: MovieTypes, newItem: MovieTypes): Boolean {
                return oldItem == newItem
            }
        })
    }


    fun submitList(list: List<MovieTypes>) {
        data.submitList(list)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMovieTypesBinding.inflate(
                LayoutInflater.from(
                    parent.context,
                ), parent, false
            ), handler
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.initView(data = data.currentList[position])
    }

    override fun getItemCount(): Int {
        return data.currentList.size
    }

    interface Handler {
        fun onPressCard(movieData: MovieData)
        fun onPressViewAll(type: String)
        fun onToggleFavorite(movieData: MovieData, differData : AsyncListDiffer<MovieData>)
    }

    class ViewHolder(
        private val binding: ItemMovieTypesBinding,
        private val handler: RvMovieTypesAdapter.Handler?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        var data: MovieTypes? = null

        fun initView(data: MovieTypes) {
            this.data = data
            initTitle()
            initMovies()
            initHandler()
        }

        private fun initHandler() {
            val data = data ?: return
            binding.handler = object : Handler {
                override fun onPressViewAll() {
                    val type = data.type ?: return
                    handler?.onPressViewAll(type)
                }
            }
        }

        private fun initMovies() {
            val movies = data?.response ?: return
            binding.rvMovies.adapter = RvMovieCardAdapter(handler).also {
                it.submitList(movies)
            }
        }

        private fun initTitle() {
            val type = data?.type ?: return
            val context = binding.root.context
            binding.title = when (type) {
                AppConstants.MovieType.HORROR -> {
                    String.format("%s \uD83D\uDC7B", context.getString(R.string.horror))
                }
                AppConstants.MovieType.ACTION -> {
                    String.format("%s \uD83D\uDD25", context.getString(R.string.action))
                }
                AppConstants.MovieType.COMEDY -> {
                    String.format("%s \uD83D\uDE02", context.getString(R.string.comedy))
                }
                else -> {
                    type
                }
            }
        }

        interface Handler {
            fun onPressViewAll()
        }

    }


}