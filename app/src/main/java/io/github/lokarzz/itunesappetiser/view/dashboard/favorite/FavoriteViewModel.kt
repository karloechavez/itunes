package io.github.lokarzz.itunesappetiser.view.dashboard.favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.github.lokarzz.itunesappetiser.repository.app.AppRepository
import io.github.lokarzz.itunesappetiser.repository.app.model.base.UIState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {


    private val _uiState by lazy {
        MutableStateFlow(FavoriteUiState())
    }
    val uiState by lazy {
        _uiState.asStateFlow()
    }

    fun fetchFavorites() {
        viewModelScope.launch {
            appRepository.fetchFavoriteMovies().collect { state ->
                when (state.status) {
                    UIState.Status.SUCCESS -> {
                        _uiState.update {
                            it.copy(movies = state.data)
                        }
                    }
                    else -> {
                        // do nothing
                    }
                }
            }
        }
    }

    fun removeFavorite(trackId: Long) {
        viewModelScope.launch {
            appRepository.toggleFavorite(trackId).collect()
            val movies = _uiState.value.movies?.filterNot { it.trackId == trackId }
            _uiState.update { it.copy(movies = movies) }
        }
    }

}