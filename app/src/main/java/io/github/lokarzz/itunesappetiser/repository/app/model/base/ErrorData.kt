package io.github.lokarzz.itunesappetiser.repository.app.model.base

data class ErrorData(var errorMessage: String? = null) {
}