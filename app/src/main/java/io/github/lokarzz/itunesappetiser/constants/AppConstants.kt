package io.github.lokarzz.itunesappetiser.constants

object AppConstants {


    object Date {
        const val DATE_ISO = "yyyy-MM-dd'T'HH:mm:ss"
        const val MM_YYYY = "MMM yyyy"
        const val MM_DD_YYYY = "MMM dd yyyy"
    }


    object MovieType {
        const val ACTION = "action"
        const val HORROR = "horror"
        const val COMEDY = "comedy"
    }

    object Bundle {
        const val SEARCH_TYPE = "search_type"
        const val TRACK_ID = "track_id"
    }

    object Preference {
        const val APP_DATASTORE = "app_datastore"
        const val LAST_SCREEN = "last_screen"
    }
}