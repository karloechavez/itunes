package io.github.lokarzz.itunesappetiser.view.dashboard.home

import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.AsyncListDiffer
import dagger.hilt.android.AndroidEntryPoint
import io.github.lokarzz.itunesappetiser.constants.AppConstants
import io.github.lokarzz.itunesappetiser.databinding.FragmentHomeBinding
import io.github.lokarzz.itunesappetiser.extensions.CoroutineExtension.observeUiState
import io.github.lokarzz.itunesappetiser.extensions.ViewExtensions.popUpScreen
import io.github.lokarzz.itunesappetiser.repository.app.model.movies.search.MovieData
import io.github.lokarzz.itunesappetiser.view.base.BaseFragment
import io.github.lokarzz.itunesappetiser.view.dashboard.home.adapter.RvMovieTypesAdapter
import io.github.lokarzz.itunesappetiser.view.moviedetail.MovieDetailsActivity
import io.github.lokarzz.itunesappetiser.view.search.SearchMovieActivity

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private var uiState: HomeUiState? = null
    private val viewModel by viewModels<HomeViewModel>()
    private val rvMovieTypesAdapter by lazy {
        RvMovieTypesAdapter()
    }
    private var differData: AsyncListDiffer<MovieData>? = null

    override fun initViewBinding(container: ViewGroup?): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(layoutInflater, container, false)
    }

    override fun initView() {
        observeLiveData()
        initAdapter()
    }

    private fun observeLiveData() {
        observeUiState(viewModel.uiState) {
            uiState = it
            initMovieTypes()
            initToggleFavorite()
        }
    }

    private fun initToggleFavorite() {
        val toggledFavorite = uiState?.toggledFavorite ?: return
        val updatedList = differData?.currentList?.map {
            if (it.trackId == toggledFavorite.trackId) toggledFavorite else it
        }
        differData?.submitList(updatedList)
    }


    private fun fetchMovieTypes() {
        viewModel.fetchMovieType(AppConstants.MovieType.ACTION)
        viewModel.fetchMovieType(AppConstants.MovieType.HORROR)
        viewModel.fetchMovieType(AppConstants.MovieType.COMEDY)
    }

    private fun initMovieTypes() {
        val movieTypes = uiState?.movieTypes ?: return
        rvMovieTypesAdapter.submitList(movieTypes)
    }

    private fun initAdapter() {
        binding.rvMovies.adapter = rvMovieTypesAdapter
        initAdapterHandler()
    }

    private fun initAdapterHandler() {
        rvMovieTypesAdapter.handler = object : RvMovieTypesAdapter.Handler {
            override fun onPressCard(movieData: MovieData) {
                popUpScreen(
                    MovieDetailsActivity::class.java,
                    bundleOf(AppConstants.Bundle.TRACK_ID to movieData.trackId)
                )
            }

            override fun onPressViewAll(type: String) {
                popUpScreen(
                    SearchMovieActivity::class.java,
                    bundleOf(AppConstants.Bundle.SEARCH_TYPE to type)
                )
            }

            override fun onToggleFavorite(
                movieData: MovieData,
                differData: AsyncListDiffer<MovieData>
            ) {
                this@HomeFragment.differData = differData
                viewModel.toggleFavorite(movieData.trackId ?: 0)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchMovieTypes()
    }


    interface Handler

}